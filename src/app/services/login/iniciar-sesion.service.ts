import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IniciarSesionModelo } from 'src/app/models/login/IniciarSesion.model';

@Injectable({
  providedIn: 'root'
})
export class IniciarSesionService {

  url:string = 'http://localhost:3000/api';

  constructor(private http:HttpClient) { }

  login(modelo:IniciarSesionModelo){
    return this.http.post(this.url + '/login', modelo);
  }
}
