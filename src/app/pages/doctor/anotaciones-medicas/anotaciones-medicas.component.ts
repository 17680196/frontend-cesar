import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-anotaciones-medicas',
  templateUrl: './anotaciones-medicas.component.html',
  styleUrls: ['./anotaciones-medicas.component.css']
})
export class AnotacionesMedicasComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  
  hojaPaciente(){
    this.router.navigate(['/dashboard-doctor/hoja-anotaciones']);
  }

}
