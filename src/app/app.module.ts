import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NopagesfoundComponent } from './pages/nopagesfound/nopagesfound.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardDoctorComponent } from './pages/doctor/dashboard-doctor/dashboard-doctor.component';
import { PacientesComponent } from './pages/doctor/components/pacientes/pacientes.component';
import { AnotacionesMedicasComponent } from './pages/doctor/anotaciones-medicas/anotaciones-medicas.component';
import { InsEnfermeraComponent } from './pages/doctor/ins-enfermera/ins-enfermera.component';
import { ProgCirugiasComponent } from './pages/doctor/prog-cirugias/prog-cirugias.component';
import { SolicitaEstudiosComponent } from './pages/doctor/solicita-estudios/solicita-estudios.component';
import { HojaAnotacionesComponent } from './pages/doctor/anotaciones-medicas/hoja-anotaciones/hoja-anotaciones.component';


import { HttpClientModule } from '@angular/common/http';

import { DashboardQuimicoComponent } from './pages/quimicos/dashboard-quimico/dashboard-quimico.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    NopagesfoundComponent,
    LoginComponent,
    DashboardDoctorComponent,
    PacientesComponent,
    AnotacionesMedicasComponent,
    InsEnfermeraComponent,
    ProgCirugiasComponent,
    SolicitaEstudiosComponent,
    HojaAnotacionesComponent,
    DashboardQuimicoComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
