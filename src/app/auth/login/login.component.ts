import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IniciarSesionModelo } from 'src/app/models/login/IniciarSesion.model';
import { IniciarSesionService } from 'src/app/services/login/iniciar-sesion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  personalForm:FormGroup;
  datos:any;

  constructor(private servicio: IniciarSesionService,private router:Router, private fb:FormBuilder) {

      this.personalForm = this.fb.group({
        email:['',Validators.required],
        password:['',Validators.required]
      });

   }

  ngOnInit(): void {
  }

  iniciarSesion(){
    let modelo:IniciarSesionModelo = {
      email:this.personalForm.get("email")?.value,
      password:this.personalForm.get("password")?.value
    }

    console.log(modelo);

    this.servicio.login(modelo).subscribe(data=>{
      this.datos = data;
     

      if(this.datos == "Doctor"){
        this.router.navigate(['/dashboard-doctor']);
      }
      if(this.datos == "Jefa Enfermeria"){
        this.router.navigate(['/dashboard-jefa-enfermeria']);
      }

    });

  }

}
