import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {LoginComponent} from './auth/login/login.component';
import { NopagesfoundComponent } from './pages/nopagesfound/nopagesfound.component';

/* Dashboard Doctor */
import { DashboardDoctorComponent } from './pages/doctor/dashboard-doctor/dashboard-doctor.component';
import { PacientesComponent } from './pages/doctor/components/pacientes/pacientes.component';
import { AnotacionesMedicasComponent } from './pages/doctor/anotaciones-medicas/anotaciones-medicas.component';
import { InsEnfermeraComponent } from './pages/doctor/ins-enfermera/ins-enfermera.component';
import { ProgCirugiasComponent } from './pages/doctor/prog-cirugias/prog-cirugias.component';
import { SolicitaEstudiosComponent } from './pages/doctor/solicita-estudios/solicita-estudios.component';
import { HojaAnotacionesComponent } from './pages/doctor/anotaciones-medicas/hoja-anotaciones/hoja-anotaciones.component';


import { DashboardQuimicoComponent } from './pages/quimicos/dashboard-quimico/dashboard-quimico.component';




/* Variable en general de rutas */
const routes: Routes = [
  /* ruta de login */
  {path:'', component:LoginComponent},
  { path:'login', component: LoginComponent },
   
  /* ruta de page found */
  { path:'nopagefound', component: NopagesfoundComponent},

  /* Rutas de dashboard doctor y anexas */
  { 
    path:'dashboard-doctor', 
    component:DashboardDoctorComponent, 
    children:[
        { path:'pacientes', component:PacientesComponent},
        { path: 'anotaciones-medicas', component:AnotacionesMedicasComponent},
        { path:'hoja-anotaciones', component: HojaAnotacionesComponent},
        {path: 'ins-enfermera', component:InsEnfermeraComponent},
        {path: 'prog-cirugias', component:ProgCirugiasComponent},
        {path: 'solicita-estudios', component:SolicitaEstudiosComponent},
        
        
    ] 
  },

  /* Fin ruta dashboard doctor */

  /* Dashboard enfermeria y anexas */


   {
    path:'dashboard-quimico',
    component:DashboardQuimicoComponent,
    children:[]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
